#include "stdafx.h"
#include "TestState.h"

#include "Sprite.h"

#include "ServiceLocator.h"
#include "RenderWindow.h"
#include "SpriteManager.h"

#include "InputHandler.h"
#include "DrawSystem.h"
#include "AABBColliderSystem.h"
#include "MoveSystem.h"
#include "PaddleControlSystem.h"
#include "BallControlSystem.h"

#include <iostream>
using namespace std;

namespace adm
{

TestState::TestState()
{
	// Register systems //
	mSystemsEngine.addSystem(std::make_shared<PaddleControlSystem>());
	mSystemsEngine.addSystem(std::make_shared<BallControlSystem>());
	mSystemsEngine.addSystem(std::make_shared<MoveSystem>());
	mSystemsEngine.addSystem(std::make_shared<AABBColliderSystem>());
	mSystemsEngine.addSystem(std::make_shared<DrawSystem>());

	// Create Sprites //
	auto& spriteManager = ServiceLocator<SpriteManager>::GetService();

	// Why does these get invalidated again??
	// references get invalidated, including references to pointers.....
	auto leftSpr = spriteManager.createSprite("../Assets/Textures/Test.bmp", { 0  , 0  , paddleWidth, 128 });
	auto rightSpr = spriteManager.createSprite("../Assets/Textures/Test.bmp", { 128, 0  , paddleWidth, 128 });
	auto ballSpr = spriteManager.createSprite("../Assets/Textures/Test.bmp", { 0  , 128, ballSize, ballSize });

	auto testSpr = spriteManager.createSprite("../Assets/PNGTest/tbbn2c16.png", { 0, 0, 32, 32 });

	// Add components //
	//left
	leftPaddle.addComponent(std::make_shared<PositionComponent>(vec2f{ padding, 100.f }));
	leftPaddle.addComponent(std::make_shared<SpriteComponent>(leftSpr));
	leftPaddle.addComponent(std::make_shared<AABBColliderComponent>(leftSpr->getSourceRect()));
	leftPaddle.addComponent(std::make_shared<AABBCollisionPairCallbacksComponent>());
	leftPaddle.addComponent(std::make_shared<PaddleControlsComponent>(SDLK_w, SDLK_s, 300));

	// right
	rightPaddle.addComponent(std::make_shared<PositionComponent>(vec2f{ 800.f - paddleWidth - padding, 100.f }));
	rightPaddle.addComponent(std::make_shared<SpriteComponent>(rightSpr));
	rightPaddle.addComponent(std::make_shared<AABBColliderComponent>(rightSpr->getSourceRect()));
	rightPaddle.addComponent(std::make_shared<AABBCollisionPairCallbacksComponent>());
	rightPaddle.addComponent(std::make_shared<PaddleControlsComponent>(SDLK_i, SDLK_k, 300));

	// ball
	ball.addComponent(std::make_shared<PositionComponent>(vec2f{ 800.f / 2 - ballSize / 2, 600.f / 2 - ballSize / 2 }));
	ball.addComponent(std::make_shared<SpriteComponent>(ballSpr));
	ball.addComponent(std::make_shared<AABBColliderComponent>(ballSpr->getSourceRect()));
	ball.addComponent(std::make_shared<AABBCollisionPairCallbacksComponent>());
	ball.addComponent(std::make_shared<VelocityComponent>(vec2f{ -100, 0 }));

	// testObject
	testObject.addComponent(std::make_shared<PositionComponent>(vec2f{ 800.f / 2 - ballSize / 2, 600.f / 2 - ballSize / 2 }));
	testObject.addComponent(std::make_shared<SpriteComponent>(testSpr));

	// Register objects //
	// left
	mSystemsEngine.addBehaviour(leftPaddle, "Draw");
	mSystemsEngine.addBehaviour(leftPaddle, "AABBCollision");
	mSystemsEngine.addBehaviour(leftPaddle, "PaddleControl");

	// right
	mSystemsEngine.addBehaviour(rightPaddle, "Draw");
	mSystemsEngine.addBehaviour(rightPaddle, "AABBCollision");
	mSystemsEngine.addBehaviour(rightPaddle, "PaddleControl");

	// ball
	mSystemsEngine.addBehaviour(ball, "Draw");
	mSystemsEngine.addBehaviour(ball, "AABBCollision");
	mSystemsEngine.addBehaviour(ball, "Move");
	mSystemsEngine.addBehaviour(ball, "BallControl");
	
	// testObject
	mSystemsEngine.addBehaviour(testObject, "Draw");
}

void TestState::update(DeltaTime_T dt)
{
	mSystemsEngine.update(dt);
}

void TestState::enter()
{
	cout << "Teststate enter\n";
}

void TestState::exit()
{
	cout << "Teststate exit\n";
}

}