#pragma once

#include "ISystem.h"
#include "PositionComponent.h"
#include "VelocityComponent.h"

namespace adm
{

class MoveSystem : public ISystem
{
public:
	MoveSystem()
		: ISystem("Move", { "Velocity", "Position" })
	{}

	void update(DeltaTime_T dt) override;

private:
};

} // namespace adm