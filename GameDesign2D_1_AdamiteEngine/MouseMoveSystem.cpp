#include "stdafx.h"
#include "MouseMoveSystem.h"

#include "InputHandler.h"

namespace adm
{

void MouseMoveSystem::update(DeltaTime_T dt)
{
	for (auto& obj : mGameObjects)
	{
		auto& pc = std::static_pointer_cast<PositionComponent>(obj.get().getComponent("Position"));
		pc->mPos.x = static_cast<float>(InputHandler::Mouse::x);
		pc->mPos.y = static_cast<float>(InputHandler::Mouse::y);
	}
}

} // namespace adm
