#include "stdafx.h"
#include "ISystem.h"

#include <cassert>

namespace adm
{

void ISystem::registerGameObject(const GameObject& pGO)
{
	/*if (!hasRequirements(pGO))
	{
		return;
	}*/
	assert(hasRequirements(pGO));

	mGameObjects.push_back(pGO);
}

bool ISystem::hasRequirements(const GameObject& pGO) const
{
	for (auto& requirement : mRequirements)
	{
		if (!pGO.hasComponent(requirement))
		{
			return false;
		}
	}

	return true;
}

}