#include "stdafx.h"
#include "SpriteManager.h"

#include "ServiceLocator.h"
#include "RenderWindow.h"

namespace adm
{

const SpritePtr SpriteManager::createSprite(const std::string& pFilename, const SDL_Rect& srcRect)
{
	// reallocation of vector causes invalidation of references..
	mSprites.push_back( std::make_shared<Sprite>( loadTexture(pFilename), srcRect ));

	return mSprites.back();
}

// Code taken and modified from Jerry's Augustus engine
const sdl2::TexturePtr& SpriteManager::loadTexture(const std::string& pFilename)
{
	auto it = mTextures.find(pFilename); // Returns an iterator to a pos or end, depending on success
	if (it == mTextures.end())
	{
		// If we do not find the texture we need to load it and inser it in to our std::map so
		// that we may create pointers to the same texture for several sprites;
		const auto& xRenderer = ServiceLocator<RenderWindow>::GetService().getRendererPtr();
		mTextures[pFilename] = sdl2::loadTextureIMG(xRenderer, pFilename);
		it = mTextures.find(pFilename);
	}

	return it->second;
}

} // namespace adm