#pragma once

#include "ISystem.h"

namespace adm
{

class SystemEngine
{
public:
	void addSystem(SystemShPtr pSystem);
	SystemShPtr& getSystem(const std::string& pBehaviour) const;
	void update(duration<double> dt);

private:
	std::vector<SystemShPtr> mSystems;
};

}