#include "stdafx.h"
#include "GameObject.h"

#include <cassert>

namespace adm
{

int GameObject::curID = 0;

bool GameObject::hasComponent(const std::string& pCompType) const
{
	for (auto& comp : mComponents)
	{
		if (comp->mType == pCompType)
		{
			return true;
		}
	}

	return false;
}

void GameObject::addComponent(ComponentShPtr pxComp)
{
	//if (hasComponent(pxComp->mType))
	//{
	//	return; // Error?
	//}
	assert(!hasComponent(pxComp->mType));
	
	mComponents.push_back(pxComp);
}

ComponentShPtr GameObject::getComponent(const std::string& pCompType) const
{
	for (auto& comp : mComponents)
	{
		if (comp->mType == pCompType)
		{
			return comp;
		}
	}

	return nullptr;
}

} // namespace adm