#pragma once

#include "ISystem.h"

#include "AABBCollisionPairCallbacksComponent.h"

namespace adm
{

class BallControlSystem : public ISystem
{
public:
	BallControlSystem()
		: ISystem("BallControl", { "Velocity", "AABBCollisionPairCallbacks" })
	{}

	void update(DeltaTime_T dt) override;
	void registerGameObject(const GameObject& pGO) override;

private:
	void onCollision(CollisionPair& pair);

	std::vector<CollisionPair> mCollisions;
};

}