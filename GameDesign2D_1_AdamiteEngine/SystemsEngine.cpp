#include "stdafx.h"
#include "SystemsEngine.h"

#include <cassert>

namespace adm
{

void SystemsEngine::update(DeltaTime_T dt)
{
	for (auto& system : mSystems)
	{
		system->update(dt);
	}
}

void SystemsEngine::addSystem(SystemShPtr pSystem)
{
	//if (hasSystem(pSystem->mBehaviour))
	//{
	//	return; // Error?
	//}
	assert(!hasSystem(pSystem->mBehaviour));

	mSystems.push_back(pSystem);
}

bool SystemsEngine::hasSystem(const std::string& pBehaviour) const
{
	for (auto& system : mSystems)
	{
		if (system->mBehaviour == pBehaviour)
		{
			return true;
		}
	}

	return false;
}

GameObject& SystemsEngine::newGameObject()
{
	mGameObjects.push_back(GameObject{});
	return mGameObjects.back();
}

void SystemsEngine::addBehaviour(const GameObject& pGO, const std::string& pBehaviour)
{
	for (auto& system : mSystems)
	{
		if (system->mBehaviour == pBehaviour)
		{
			system->registerGameObject(pGO);
			return;
		}
	}

	assert(false);
}

} // namespace adm