#pragma once

#include <vector>

#include "ISystem.h"
#include "PositionComponent.h"

namespace adm
{

class MouseMoveSystem : public ISystem
{
public:
	MouseMoveSystem()
		: ISystem("MouseMove", { "Position" })
	{}

	void update(DeltaTime_T dt) override;
};

} // namespace adm