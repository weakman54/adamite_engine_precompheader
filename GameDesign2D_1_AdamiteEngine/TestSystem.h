#pragma once

#include "ISystem.h"
#include "AABBCollisionPairCallbacksComponent.h"

namespace adm
{

class TestSystem : public ISystem
{
public:
	TestSystem()
		: ISystem("Test", { "AABBCollisionPairCallbacks" })
	{}

	void update(DeltaTime_T dt) override;
	void registerGameObject(const GameObject& pGO);

private:
	std::vector<CollisionPair> mPairs;

	void onCollision(CollisionPair& pair);
};

}