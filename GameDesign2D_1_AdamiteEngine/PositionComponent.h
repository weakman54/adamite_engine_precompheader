#pragma once

#include "IComponent.h"
#include "vec2.h"

namespace adm
{

struct PositionComponent : public IComponent
{
	PositionComponent(const vec2f& pPos = { 0, 0 })
		: IComponent("Position")
		, mPos{ pPos }
	{}

	vec2f mPos;
};

} // namespace adm