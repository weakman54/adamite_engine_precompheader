#pragma once



class InputHandler
{
public:
	// State structs
	struct Keyboard
	{
		static const Uint8* keyboardState;

		static bool keyDown(SDL_Scancode scancode);
		static bool keyDown(SDL_Keycode  keycode );
	};

	struct Mouse
	{
		static int x;
		static int y;

		static bool buttonDown(int button);
	};

	InputHandler() = delete;

	static void handleMouseMove(const SDL_MouseMotionEvent& event);

private:
};
