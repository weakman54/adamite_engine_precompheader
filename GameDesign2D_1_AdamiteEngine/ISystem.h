#pragma once

#include <vector>
#include <memory>
#include <chrono>
using namespace std::chrono;

#include "GameObject.h"

namespace adm
{

class ISystem
{
public:
	ISystem(const std::string& pBehaviour, const std::vector<std::string>& pRequirements)
		: mBehaviour{ pBehaviour }
		, mRequirements{ pRequirements }
	{}
	virtual ~ISystem() = default;

	virtual void update(DeltaTime_T dt) = 0;

	virtual void registerGameObject(const GameObject& pGO);
	virtual bool hasRequirements(const GameObject& pGO) const;

	const std::string mBehaviour;
	const std::vector<std::string> mRequirements;

protected:
	std::vector<std::reference_wrapper<const GameObject>> mGameObjects;
};

using SystemShPtr = std::shared_ptr<ISystem>;

} // namespace adm