#pragma once

#include "IState.h"
#include "GameObject.h"
#include "SystemsEngine.h"

namespace adm
{

class TestState : public IState
{
public:
	TestState();

	void update(DeltaTime_T dt) override;
	void enter() override;
	void exit() override;

private:
	GameObject leftPaddle;
	GameObject rightPaddle;
	GameObject ball;
	GameObject testObject;
	SystemsEngine mSystemsEngine;

	const int paddleWidth = 30;
	const int ballSize = 24;
	const float padding = 30;
};

}
