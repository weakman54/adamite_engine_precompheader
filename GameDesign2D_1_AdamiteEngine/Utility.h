#pragma once

#include <memory>

// Code from http://swarminglogic.com/jotting/2015_05_smartwrappers
// Thanks to Ulf Benjaminson for reminding me of this!

namespace sdl2
{

// Deleter functor
struct SDL_Deleter
{
	void operator()(SDL_Surface*  ptr) { if (ptr) SDL_FreeSurface(ptr); }
	void operator()(SDL_Texture*  ptr) { if (ptr) SDL_DestroyTexture(ptr); }
	void operator()(SDL_Renderer* ptr) { if (ptr) SDL_DestroyRenderer(ptr); }
	void operator()(SDL_Window*   ptr) { if (ptr) SDL_DestroyWindow(ptr); }
	void operator()(SDL_RWops*    ptr) { if (ptr) SDL_RWclose(ptr); }
};

// Unique Pointers

using WindowPtr   = std::unique_ptr<SDL_Window  , SDL_Deleter>;
using RendererPtr = std::unique_ptr<SDL_Renderer, SDL_Deleter>;
using SurfacePtr  = std::unique_ptr<SDL_Surface , SDL_Deleter>;
using TexturePtr  = std::unique_ptr<SDL_Texture , SDL_Deleter>;
using RWopsPtr    = std::unique_ptr<SDL_RWops   , SDL_Deleter>;



// Shared ptr with deleter

template<class T, class D = std::default_delete<T>>
struct shared_ptr_with_deleter : public std::shared_ptr<T>
{
	explicit shared_ptr_with_deleter(T* t = nullptr)
		: std::shared_ptr<T>{ t, D() }
	{}

	void reset(T* t = nullptr)
	{
		std::shared_ptr<T>::reset(t, D());
	}
};

// Shared pointers

using SurfaceShPtr = shared_ptr_with_deleter<SDL_Surface, SDL_Deleter>;
using TextureShPtr = shared_ptr_with_deleter<SDL_Texture, SDL_Deleter>;
using RendererShPtr = shared_ptr_with_deleter<SDL_Renderer, SDL_Deleter>;
using WindowShPtr = shared_ptr_with_deleter<SDL_Window, SDL_Deleter>;
using RWopsShPtr = shared_ptr_with_deleter<SDL_RWops, SDL_Deleter>;



// SDL creator-function wrappers
// TODO: inline?

WindowPtr createWindow(const std::string& title, int width, int height, 
					   int x = SDL_WINDOWPOS_UNDEFINED,
					   int y = SDL_WINDOWPOS_UNDEFINED,
					   Uint32 flags = SDL_WINDOW_OPENGL);

RendererPtr createRenderer(const WindowPtr&, Uint32 flags = SDL_RENDERER_ACCELERATED, int index = -1);

SurfacePtr loadSurfaceFromBMP(const std::string& pFilename);
SurfacePtr loadSurfaceIMG(const std::string& pFilename); // Uses SDL_image library

TexturePtr createTextureFromSurface(const RendererPtr& pxRenderer, const SurfacePtr& pxSurface);

TexturePtr loadTextureFromBMP(const RendererPtr& pxRenderer, const std::string& pFilename);
TexturePtr loadTextureIMG(const RendererPtr& pxRenderer, const std::string& pFilename); // Uses SDL_image library

} // namespace sdl2
