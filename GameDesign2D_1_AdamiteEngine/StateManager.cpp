/*
Code originally made by Jerry Jonsson, and licensed under Apache 2.0
Modifications by Erik Wallin
*/

#include "stdafx.h"
#include "StateManager.h"

namespace adm
{

// R-value and move semantics in ctor grabbed from:
// http://stackoverflow.com/questions/8114276/how-do-i-pass-a-unique-ptr-argument-to-a-constructor-or-a-function
StateManager::StateManager(const std::string& initialName, StatePtr&& initialState)
	: mCurrentStateKey{ initialName }
{
	mStates[initialName] = std::move(initialState);
	mStates[initialName]->enter();
}

void StateManager::changeState(const std::string & to)
{
	//auto& t = mStates.find(mCurrentStateKey);

	if (mStates.find(to) != mStates.end() && to != mCurrentStateKey)
	{
		mStates[mCurrentStateKey]->exit();
		mCurrentStateKey = to;
		mStates[mCurrentStateKey]->enter();
	}
	else
	{
		throw;
	}
}

void StateManager::update(DeltaTime_T dt) const
{
	mStates.at(mCurrentStateKey)->update(dt);
}

void StateManager::addState(const std::string & name, StatePtr state)
{
	mStates[name] = std::move(state);
	// TODO: report non insert?
}

} // namespace adm
