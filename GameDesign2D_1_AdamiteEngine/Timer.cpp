#include "stdafx.h"
#include "Timer.h"

#include <iostream>
using namespace std;

DeltaTime_T Timer::getDelta()
{
	auto timeCurrent = high_resolution_clock::now();
	DeltaTime_T deltaTime(timeCurrent - timePrev);
	timePrev = high_resolution_clock::now();

	mLatestDeltas[mCurIndex++] = deltaTime;
	//
	if (mCurIndex >= mLatestDeltas.size())
	{
		cout << static_cast<int>(calculateFPS()) << endl;
	}
	//
	mCurIndex %= mLatestDeltas.size();

	return deltaTime;
}

double Timer::calculateFPS()
{
	DeltaTime_T total{ 0 };

	for (auto& delta : mLatestDeltas)
	{
		total += delta;
	}

	avgFPS = mLatestDeltas.size() / total.count();

	return avgFPS;
}
