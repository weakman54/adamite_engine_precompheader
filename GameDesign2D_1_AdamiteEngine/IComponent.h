#pragma once

#include <memory>

namespace adm
{

struct IComponent
{
public:
	IComponent(const std::string& pType)
		: mType{ pType }
	{}
	virtual ~IComponent() = default;

	const std::string mType;
};

using ComponentShPtr = std::shared_ptr<IComponent>;

} // namespace adm