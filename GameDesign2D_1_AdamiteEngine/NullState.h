#pragma once

#include "IState.h"

namespace adm
{

class NullState : public IState
{
	void update(DeltaTime_T dt) override {}
};

}