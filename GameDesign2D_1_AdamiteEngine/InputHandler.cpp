#include "stdafx.h"
#include "InputHandler.h"

const Uint8* InputHandler::Keyboard::keyboardState = SDL_GetKeyboardState(nullptr);

bool InputHandler::Keyboard::keyDown(SDL_Scancode scancode)
{
	return keyboardState[scancode] == 1;
}

bool InputHandler::Keyboard::keyDown(SDL_Keycode keycode)
{
	return keyDown(SDL_GetScancodeFromKey(keycode));
}


int InputHandler::Mouse::x = 0;
int InputHandler::Mouse::y = 0;

bool InputHandler::Mouse::buttonDown(int button)
{
	return SDL_GetMouseState(nullptr, nullptr) & SDL_BUTTON(button);
}

void InputHandler::handleMouseMove(const SDL_MouseMotionEvent& event)
{
	Mouse::x = event.x;
	Mouse::y = event.y;
}
