/*
Code taken from assignment 1 in an earlier course (5sd803)
Modified to work with the Adamite engine
*/
#pragma once

#include <map>

#include "Istate.h"
namespace adm
{

class StateManager
{
public:
	StateManager(const std::string& initialName, StatePtr&& initialState);
	virtual ~StateManager() = default;

	void changeState(const std::string& to);
	void update(DeltaTime_T dt) const;
	void addState(const std::string& name, StatePtr state);

private:
	std::string mCurrentStateKey;
	std::map<std::string, StatePtr> mStates;
};

}