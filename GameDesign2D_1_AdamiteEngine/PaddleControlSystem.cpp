#include "stdafx.h"
#include "PaddleControlSystem.h"

#include "InputHandler.h"

namespace adm
{

void PaddleControlSystem::update(DeltaTime_T dt)
{
	for (auto& node : mNodes)
	{
		auto& posC = node.posComp.lock();
		auto& conC = node.controlsComp.lock();

		if (InputHandler::Keyboard::keyDown(conC->mDownKey))
		{
			posC->mPos.y += conC->mSpeed * dt.count();
		}
		else if (InputHandler::Keyboard::keyDown(conC->mUpKey))
		{
			posC->mPos.y -= conC->mSpeed * dt.count();
		}
	}
}

void PaddleControlSystem::registerGameObject(const GameObject & pGO)
{
	if (!hasRequirements(pGO))
	{
		return; // Error?
	}

	mNodes.push_back(PaddleControlNode{ 
		std::static_pointer_cast<PaddleControlsComponent>(pGO.getComponent("PaddleControls")),
		std::static_pointer_cast<PositionComponent>(pGO.getComponent("Position"))
	});
}

} // namespace adm
