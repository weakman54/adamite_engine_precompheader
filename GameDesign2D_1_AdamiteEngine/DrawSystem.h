#pragma once

#include <vector>

#include "ISystem.h"

#include "ServiceLocator.h"
#include "RenderWindow.h"

#include "PositionComponent.h"
#include "SpriteComponent.h"

namespace adm
{

class DrawSystem : public ISystem
{
public:
	DrawSystem()
		: ISystem{ "Draw", { "Sprite", "Position" }}
		, mRenderWindow{ ServiceLocator<RenderWindow>::GetService() }
	{}

	void update(DeltaTime_T dt) override;

private:
	const RenderWindow& mRenderWindow;
};

}