#include "stdafx.h"
#include "DrawSystem.h"

namespace adm
{

void DrawSystem::update(DeltaTime_T dt)
{
	mRenderWindow.clear({ 0, 0, 0 });

	for (auto& obj : mGameObjects)
	{
		auto& sc = std::static_pointer_cast<SpriteComponent>(obj.get().getComponent("Sprite"));
		auto& pc = std::static_pointer_cast<PositionComponent>(obj.get().getComponent("Position"));
		
		mRenderWindow.drawSprite(sc->sprite, (int)pc->mPos.x, (int)pc->mPos.y);
	}

	mRenderWindow.present();
}

}