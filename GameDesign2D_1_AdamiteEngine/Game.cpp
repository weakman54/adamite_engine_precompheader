#include "stdafx.h"
#include "Game.h"

//
#include "ServiceLocator.h"
#include "InputHandler.h"

#include "NullState.h"
#include "TestState.h"

#include <iostream>
using namespace std;
//

namespace adm
{

Game::Game()
	: mSDLSystem{}
	, mRenderWindow{ "SDL Thing", 800, 600 }
	, mRunning{ true }
	///////////////
	, mStateManager{ "Null", std::make_unique<NullState>( ) }
	//////////////
{
	ServiceLocator<RenderWindow>::SetService(&mRenderWindow);
	ServiceLocator<SpriteManager>::SetService(&mSpriteManager);

	mStateManager.addState("Test", std::make_unique<TestState>());
	mStateManager.changeState("Test");
}

void Game::run()
{
	while (mRunning)
	{
		mDeltaTime = mTimer.getDelta();

		processEvents();
		mStateManager.update(mDeltaTime);
	}
}

void Game::processEvents()
{
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			mRunning = false;
			break;
		case SDL_MOUSEMOTION:
			InputHandler::handleMouseMove(event.motion);
			break;
		default:
			break;
		}
	}
}

} // namespace adm
