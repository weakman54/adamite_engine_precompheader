#include "stdafx.h"
#include "TestSystem.h"

#include <functional>
using namespace std::placeholders;

#include "PositionComponent.h"

namespace adm
{

void TestSystem::update(DeltaTime_T dt)
{
	for (auto& obj : mGameObjects)
	{
		// Unsafe! position comp not a req for this system!
		auto& posComp = std::static_pointer_cast<PositionComponent>(obj.get().getComponent("Position"));

		if (mPairs.size() > 0)
		{
			posComp->mPos += vec2f{ 100, 100 };
		}

		mPairs.clear();
	}
}

void TestSystem::registerGameObject(const GameObject& pGO)
{
	ISystem::registerGameObject(pGO);

	auto& callbackComp = std::static_pointer_cast<AABBCollisionPairCallbacksComponent>(mGameObjects.back().get().getComponent("AABBCollisionPairCallbacks"));
	callbackComp.get()->mCallbacks.push_back(std::bind(&TestSystem::onCollision, this, _1));
}

void TestSystem::onCollision(CollisionPair& pair)
{
	mPairs.push_back(pair);
}

}
