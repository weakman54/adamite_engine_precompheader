#pragma once

#include <map>
#include <vector>

#include "Utility.h"
#include "Sprite.h"

namespace adm
{

class RenderWindow;

class SpriteManager
{
public:
	const SpritePtr createSprite(const std::string& pFilename, const SDL_Rect& srcRect = { 0, 0, 0, 0 });
	const sdl2::TexturePtr& loadTexture(const std::string& pFilename);

private:
	// TODO: could use hashmap or something here, check if optimizes
	std::map<std::string, sdl2::TexturePtr> mTextures;
	std::vector<SpritePtr> mSprites;
};

} // namespace adm