#pragma once

#include "ISystem.h"
#include "PaddleControllerComponent.h"
#include "PositionComponent.h"

namespace adm
{

class PaddleControlSystem : public ISystem
{
public:
	PaddleControlSystem()
		: ISystem("PaddleControl", { "PaddleControls", "Position" })
	{}

	void update(DeltaTime_T dt) override;
	void registerGameObject(const GameObject& pGO) override;

private:
	struct PaddleControlNode
	{
		PaddleControlNode(std::weak_ptr<PaddleControlsComponent> pControls,
			              std::weak_ptr<PositionComponent> pPos)
			: controlsComp{ pControls }
			, posComp{ pPos }
		{}

		std::weak_ptr<PaddleControlsComponent> controlsComp;
		std::weak_ptr<PositionComponent> posComp;
	};

	std::vector<PaddleControlNode> mNodes;
};

}