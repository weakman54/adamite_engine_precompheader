#pragma once

#include <vector>

#include "SDLSystem.h"
#include "RenderWindow.h"
#include "SpriteManager.h"
#include "Timer.h"

//
#include "StateManager.h"
//

namespace adm
{
class Game
{
public:
	Game();

	void run();

private:
	SDLSystem mSDLSystem;
	RenderWindow mRenderWindow;
	SpriteManager mSpriteManager;
	Timer mTimer;

	DeltaTime_T mDeltaTime;
	bool mRunning;

	void processEvents();

	/////////////
	StateManager mStateManager;
	//////////////
};

} // namespace adm