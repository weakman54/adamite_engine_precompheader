#pragma once

#include <vector>

#include "IComponent.h"

namespace adm
{

class GameObject
{
public:
	GameObject(const std::vector<ComponentShPtr>& pComponents = {})
		: mComponents{ pComponents }
		, mID{ curID++ }
	{}

	bool hasComponent(const std::string& pCompType) const;
	void addComponent(ComponentShPtr pxComp);
	ComponentShPtr getComponent(const std::string& pCompType) const;
	
	const int mID;

private:
	std::vector<ComponentShPtr> mComponents;
	
	static int curID;
};

} // namespace adm