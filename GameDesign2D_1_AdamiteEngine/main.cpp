#include "stdafx.h"

#include <iostream>

#include "Game.h"

int main(int argc, char* argv[])
{
	try
	{
		adm::Game game;
		game.run();
	} 
	catch (const std::exception& err)
	{
		std::cout << err.what() << std::endl;
		return 1;
	} 
	catch (...)
	{
		std::cout << "Something exceptionally exceptional happened!" << std::endl;
		return 2;
	}

    return 0;
}