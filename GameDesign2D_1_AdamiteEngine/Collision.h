#pragma once

#include "SDL.h"

namespace sdl2
{

// code taken and modified from Jerrys Augustus engine
bool AABBCollision(const SDL_Rect& pFirst, const SDL_Rect& pSecond)
{
	const int first_left = pFirst.x;
	const int first_right = pFirst.x + pFirst.w;
	const int first_up = pFirst.y;
	const int first_down = pFirst.y + pFirst.h;

	const int second_left = pSecond.x;
	const int second_right = pSecond.x + pSecond.w;
	const int second_up = pSecond.y;
	const int second_down = pSecond.y + pSecond.h;

	if (first_right  <  second_left ||
		second_right <  first_left ||
		first_down   <  second_up ||
		second_down  <  first_up)
	{
		return false;
	}
	return true;
}

}