#include "stdafx.h"
#include "RenderWindow.h"

namespace adm
{

RenderWindow::RenderWindow(const std::string& title, int width, int height)
	: mxWindow{ sdl2::createWindow(title, width, height) }
	, mxRenderer{ sdl2::createRenderer(mxWindow) }
{}

void RenderWindow::setColor(const SDL_Color& color) const
{
	SDL_SetRenderDrawColor(mxRenderer.get(), color.r, color.b, color.b, color.a);
}

void RenderWindow::clear() const
{
	SDL_RenderClear(mxRenderer.get());
}

void RenderWindow::clear(const SDL_Color& color) const
{
	setColor(color);
	clear();
}

void RenderWindow::present() const
{
	SDL_RenderPresent(mxRenderer.get());
}

void RenderWindow::copy(const sdl2::TexturePtr& texture, const SDL_Rect& srcRect, const SDL_Rect& dstRect) const
{
	SDL_RenderCopy(mxRenderer.get(), texture.get(), &srcRect, &dstRect);
}

void RenderWindow::drawSprite(const SpritePtr& pSprite, int x, int y) const
{
	SDL_Rect dstRect{ x, y, pSprite->getSourceRect().w, pSprite->getSourceRect().h };

	copy(pSprite->getTexturePtr(), pSprite->getSourceRect(), dstRect);
}

const sdl2::RendererPtr& RenderWindow::getRendererPtr() const
{
	return mxRenderer;
}

} // namespace adm