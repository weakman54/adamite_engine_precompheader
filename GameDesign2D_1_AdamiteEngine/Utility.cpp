#include "stdafx.h"
#include "Utility.h"

namespace sdl2
{

WindowPtr createWindow(const std::string & title, int width, int height, int x, int y, Uint32 flags)
{
	SDL_Window* window = SDL_CreateWindow(title.c_str(), x, y, width, height, flags);

	if (!window)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return WindowPtr(window);
}

RendererPtr createRenderer(const WindowPtr& pxWindow, Uint32 flags, int index)
{
	SDL_Renderer* renderer = SDL_CreateRenderer(pxWindow.get(), index, flags);

	if (!renderer)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return RendererPtr(renderer);
}

SurfacePtr loadSurfaceFromBMP(const std::string& pFilename)
{
	SDL_Surface* surface = SDL_LoadBMP(pFilename.c_str());

	if (!surface)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return SurfacePtr(surface);
}

SurfacePtr loadSurfaceIMG(const std::string& pFilename)
{
	SDL_Surface* surface = IMG_Load(pFilename.c_str());

	if (!surface)
	{
		throw std::runtime_error(IMG_GetError());
	}

	return SurfacePtr(surface);
}

TexturePtr createTextureFromSurface(const RendererPtr& pxRenderer, const SurfacePtr& pxSurface)
{
	SDL_Texture* texture = SDL_CreateTextureFromSurface(pxRenderer.get(), pxSurface.get());

	if (!texture)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return TexturePtr(texture);
}

TexturePtr loadTextureFromBMP(const RendererPtr& pxRenderer, const std::string& pFilename)
{
	return createTextureFromSurface(pxRenderer, loadSurfaceFromBMP(pFilename));
}

TexturePtr loadTextureIMG(const RendererPtr& pxRenderer, const std::string & pFilename)
{
	return createTextureFromSurface(pxRenderer, loadSurfaceIMG(pFilename));
}

} // namespace sdl2