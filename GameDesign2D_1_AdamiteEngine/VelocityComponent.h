#pragma once

#include "IComponent.h"
#include "vec2.h"

namespace adm
{

struct VelocityComponent : public IComponent
{
public:
	VelocityComponent(const vec2f& pVel)
		: IComponent("Velocity")
		, mVel{ pVel }
	{}

	vec2f mVel;
};

}