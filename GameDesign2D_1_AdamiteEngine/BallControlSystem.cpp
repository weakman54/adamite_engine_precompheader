#include "stdafx.h"
#include "BallControlSystem.h"

#include <functional>
using namespace std::placeholders;

#include "VelocityComponent.h"

void adm::BallControlSystem::update(DeltaTime_T dt)
{
	if (mCollisions.size() > 0)
	{
		for (auto& obj : mGameObjects)
		{
			auto& vel = std::static_pointer_cast<VelocityComponent>(obj.get().getComponent("Velocity"))->mVel;

			if (mCollisions[0].second.mID == 0) // Hardcoded left paddle!!
			{
				vel.x = fabsf(vel.x);
			}
			else // right paddle
			{
				vel.x = -fabsf(vel.x);
			}
		}

		mCollisions.clear();
	}
}

void adm::BallControlSystem::registerGameObject(const GameObject& pGO)
{
	ISystem::registerGameObject(pGO);

	auto callbackComp = std::static_pointer_cast<AABBCollisionPairCallbacksComponent>
		(mGameObjects.back().get().getComponent("AABBCollisionPairCallbacks"));

	callbackComp->mCallbacks.push_back(std::bind(&BallControlSystem::onCollision, this, _1));
}

void adm::BallControlSystem::onCollision(CollisionPair& pair)
{
	mCollisions.push_back(pair);
}
