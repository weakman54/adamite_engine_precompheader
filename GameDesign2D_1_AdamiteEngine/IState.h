#pragma once

#include <memory>
#include <chrono>
using namespace std::chrono;

namespace adm
{

class IState
{
public:
	virtual ~IState() = default;

	virtual void update(DeltaTime_T dt) = 0;
	virtual void enter() {}
	virtual void exit() {}
};

using StatePtr = std::unique_ptr<IState>;

} // namespace adm