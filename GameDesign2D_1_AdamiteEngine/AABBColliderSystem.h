#pragma once

#include "ISystem.h"

#include "AABBColliderComponent.h"
#include "PositionComponent.h"
#include "AABBCollisionPairCallbacksComponent.h"

namespace adm
{

class AABBColliderSystem : public ISystem
{
public:
	AABBColliderSystem()
		: ISystem("AABBCollision", { "AABBCollider", "Position", "AABBCollisionPairCallbacks" })
	{}

	void update(DeltaTime_T dt) override;
};

} // namespace adm

