#pragma once

//TODO: Rename? onCollisionCallback using *Callback as a convention for
// either a component with funcitonality, or a functor?

#include <functional>

#include "IComponent.h"
#include "GameObject.h"

namespace adm
{

struct OnCollisionComponent : public IComponent
{
	using CallbackT = std::function<void(const GameObject&)>;

	OnCollisionComponent(const CallbackT& pCallback)
		: IComponent("OnCollision")
		, mCallback{ pCallback }
	{}

	CallbackT mCallback;
};

} // namespace adm
