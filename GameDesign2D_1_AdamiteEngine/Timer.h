#pragma once

#include <array>

// Code taken and adapted from:
// http://headerphile.com/cpp11/c11-part-2-timing-with-chrono/
// http://headerphile.com/sdl2/sdl2-part-9-no-more-delays/

class Timer
{
public:
	Timer()
		: timePrev{ high_resolution_clock::now() }
		, mCurIndex{ 0 }
		, avgFPS{ 0 }
		, mLatestDeltas{ DeltaTime_T{ 0 } }
	{}

	// Returns duration in seconds (with double precision) 
	// since last time this function was called
	DeltaTime_T getDelta();

	double calculateFPS();

	//
	std::array<DeltaTime_T, 20> mLatestDeltas;
	size_t mCurIndex;

	double avgFPS;
	//

private:
	time_point< high_resolution_clock > timePrev;
};