#pragma once

#include "IComponent.h"
#include "Sprite.h"

namespace adm
{

struct SpriteComponent : public IComponent
{
	SpriteComponent(const SpritePtr& spr)
		: IComponent("Sprite")
		, sprite{ spr }
	{}

	const SpritePtr sprite;
};

} // namespace adm