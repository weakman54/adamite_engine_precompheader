#pragma once

#include <functional>
#include <vector>

#include "IComponent.h"
#include "GameObject.h"

// Thanks to Mattias Ramqvist for being a 
// discussion companion for this implementation.

namespace adm
{

using CollisionPair = std::pair<const GameObject&, const GameObject&>;
using AABBCollisionCallback = std::function<void(CollisionPair&)>;

struct AABBCollisionPairCallbacksComponent : public IComponent
{
	AABBCollisionPairCallbacksComponent()
		: IComponent("AABBCollisionPairCallbacks")
	{}

	std::vector<AABBCollisionCallback> mCallbacks;
};

}
