#pragma once

#include "Utility.h"
#include "Sprite.h"

namespace adm
{

class RenderWindow
{
public:
	RenderWindow(const std::string& title, int width, int height);

	void setColor(const SDL_Color& color) const;
	void clear() const;
	void clear(const SDL_Color& color) const;
	void present() const;
	void copy(const sdl2::TexturePtr& texture, const SDL_Rect& srcRect, const SDL_Rect& dstRect) const;
	void drawSprite(const SpritePtr& xSprite, int x, int y) const;

	const sdl2::RendererPtr& getRendererPtr() const;

private:
	sdl2::WindowPtr mxWindow;
	sdl2::RendererPtr mxRenderer;
};

} // namespace adm
