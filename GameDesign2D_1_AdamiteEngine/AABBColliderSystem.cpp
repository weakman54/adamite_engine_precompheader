#include "stdafx.h"
#include "AABBColliderSystem.h"

#include "Collision.h"

namespace adm
{

void AABBColliderSystem::update(DeltaTime_T dt)
{
	// TODO: fix, inefficient probably!
	// Move each collbox to posComp
	for (auto& obj : mGameObjects)
	{
		auto& posC = std::static_pointer_cast<PositionComponent>(obj.get().getComponent("Position"));
		auto& boxC = std::static_pointer_cast<AABBColliderComponent>(obj.get().getComponent("AABBCollider"));

		boxC->mBoundingBox.x = static_cast<int>(posC->mPos.x);
		boxC->mBoundingBox.y = static_cast<int>(posC->mPos.y);
	}

	// Go through each unique gameObj pair and check for collision
	for (size_t firstI = 0; firstI < mGameObjects.size(); firstI++)
	{
		auto& first = mGameObjects[firstI];
		auto& firstBoxC = std::static_pointer_cast<AABBColliderComponent>(first.get().getComponent("AABBCollider"));

		for (size_t secondI = firstI + 1; secondI < mGameObjects.size(); secondI++)
		{
			auto& second = mGameObjects[secondI];
			auto& secondBoxC = std::static_pointer_cast<AABBColliderComponent>(second.get().getComponent("AABBCollider"));

			if (sdl2::AABBCollision(firstBoxC->mBoundingBox, secondBoxC->mBoundingBox))
			{
				auto& colCals = std::static_pointer_cast<AABBCollisionPairCallbacksComponent>(first.get().getComponent("AABBCollisionPairCallbacks"));
				
				for (auto& callback : colCals->mCallbacks)
				{
					callback(std::make_pair(first, second));
				}

				colCals = std::static_pointer_cast<AABBCollisionPairCallbacksComponent>(second.get().getComponent("AABBCollisionPairCallbacks"));

				for (auto& callback : colCals->mCallbacks)
				{
					callback(std::make_pair(second, first));
				}
			}
		}
	}
}

} // namespace adm
