#pragma once

#include "IComponent.h"
#include "SDL.h" // TODO: add cpp files because precomp headers?

namespace adm
{

struct AABBColliderComponent : public IComponent
{
	AABBColliderComponent(const SDL_Rect& pBBox = { 0, 0, 0, 0 })
		: IComponent("AABBCollider")
		, mBoundingBox( pBBox )
	{}

	SDL_Rect mBoundingBox;
};

}