#include "stdafx.h"
#include "MoveSystem.h"

namespace adm
{

void adm::MoveSystem::update(DeltaTime_T dt)
{
	for (auto& obj : mGameObjects)
	{
		auto& posC = std::static_pointer_cast<PositionComponent>(obj.get().getComponent("Position"));
		auto& velC = std::static_pointer_cast<VelocityComponent>(obj.get().getComponent("Velocity"));

		posC->mPos += velC->mVel * dt.count();
	}
}
} // namespace adm