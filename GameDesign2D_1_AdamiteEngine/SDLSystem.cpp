#include "stdafx.h"
#include "SDLSystem.h"

namespace adm
{
	SDLSystem::SDLSystem(Uint32 SDLFlags, int IMGFlags)
	{
		if (SDL_Init(SDLFlags) != 0)
		{
			throw std::runtime_error(SDL_GetError());
		}

		// TODO: should probably put other init in their own classes
		if ((IMG_Init(IMGFlags) & IMGFlags) != IMGFlags)
		{
			throw std::runtime_error(IMG_GetError());
		}
	}


	SDLSystem::~SDLSystem()
	{
		IMG_Quit();
		SDL_Quit();
	}
}
