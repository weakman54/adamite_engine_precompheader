#pragma once

#include "Utility.h"

namespace adm
{

class Sprite
{
public:
	Sprite(const sdl2::TexturePtr& pxTexture, const SDL_Rect& pRect = { 0, 0, 0, 0 });
	Sprite(const sdl2::TexturePtr& pxTexture, int x = 0, int y = 0, int w = 0, int h = 0);

	const sdl2::TexturePtr& getTexturePtr() const;
	const SDL_Rect& getSourceRect() const;

private:
	const sdl2::TexturePtr& mxTexture;
	SDL_Rect mSource;
};

// Shpuldn't be needed, Sprite is small and non-virtual
// Is needed, since they are stored in a vector, any references gets invalidated
// when the vector reallocates. Should change to a better collection-type later...
using SpritePtr = std::shared_ptr<Sprite>;

} // namespace adm