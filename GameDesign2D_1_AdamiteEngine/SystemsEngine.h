#pragma once

#include "ISystem.h"

namespace adm
{

class SystemsEngine
{
public:
	void update(DeltaTime_T dt);

	void addSystem(SystemShPtr pSystem);
	bool hasSystem(const std::string& pBehaviour) const;

	GameObject& newGameObject();
	void addBehaviour(const GameObject& pGO, const std::string& pBehaviour);

private:
	std::vector<SystemShPtr> mSystems;
	std::vector<GameObject>  mGameObjects;
};

} // namespace adm