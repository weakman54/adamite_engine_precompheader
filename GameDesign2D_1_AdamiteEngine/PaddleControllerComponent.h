#pragma once

#include "IComponent.h"
namespace adm
{

class PaddleControlsComponent : public IComponent
{
public:
	PaddleControlsComponent(SDL_Keycode pUpKey, SDL_Keycode pDownKey, int pSpeed)
		: IComponent("PaddleControls")
		, mUpKey{ pUpKey }
		, mDownKey{ pDownKey }
		, mSpeed{ pSpeed }
	{}

	SDL_Keycode mUpKey, mDownKey;
	int mSpeed;
};

} // namespace adm