// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, 
// but are changed infrequently

#pragma once

#include "targetver.h"


#include <stdexcept>
#include <string>
#include <memory>
#include <chrono>
using namespace std::chrono;
using DeltaTime_T = duration<double>;

#include "SDL.h"
#include "SDL_image.h"