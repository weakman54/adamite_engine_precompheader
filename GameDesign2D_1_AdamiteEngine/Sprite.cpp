#include "stdafx.h"
#include "Sprite.h"

namespace adm
{

Sprite::Sprite(const sdl2::TexturePtr& pxTexture, const SDL_Rect& pRect)
	: mxTexture{ pxTexture }
	, mSource(pRect)
{
	// TODO: Code repeat here still....
	if (mSource.w <= 0 || mSource.h <= 0)
	{
		int w = 0, h = 0;
		if (SDL_QueryTexture(mxTexture.get(), nullptr, nullptr, &w, &h) != 0)
		{
			throw std::runtime_error(SDL_GetError());
		}

		mSource.w = w;
		mSource.h = h;
	}
}

Sprite::Sprite(const sdl2::TexturePtr& pxTexture, int x, int y, int w, int h)
	: mxTexture{ pxTexture }
	, mSource{ x, y, w, h }
{
	// TODO: Code repeat here still....
	if (mSource.w <= 0 || mSource.h <= 0)
	{
		int w = 0, h = 0;
		if (SDL_QueryTexture(mxTexture.get(), nullptr, nullptr, &w, &h) != 0)
		{
			throw std::runtime_error(SDL_GetError());
		}

		mSource.w = w;
		mSource.h = h;
	}
}

const sdl2::TexturePtr& Sprite::getTexturePtr() const
{
	return mxTexture;
}

const SDL_Rect& Sprite::getSourceRect() const
{
	return mSource;
}

} // namespace adm