# README #
The Adamite engine is made in C++ and uses SDL2.
It's made for the course "Game programming in 2D" at Uppsala University, campus Gotland, GAME programme

# LICENSE #
This engine is written by Erik Wallin, but with many parts added from other sources. Attribution is
cited in comments with each function/sourcefile that is added/modified from other sources.
Much of this code was written by Jerry Jonsson for his Augustus engine, some of it is licensed under the
Apache License, which is also included here.
