
#include <iostream>

#include "Game.h"

// Put a folder called "External" next to your .sln file
// Copy SDL2-2.0.5 folder into that folder
// Copy main.cpp

int main(int argc, char* argv[])
{
	try
	{
		Game game;
		game.run();
	} 
	catch (const std::exception& err)
	{
		std::cout << err.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cout << "Something exceptionally exceptional happened!" << std::endl;
		return 2;
	}

	return 0;
}